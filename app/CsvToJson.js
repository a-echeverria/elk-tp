const fs = require('fs');
const csvToJson = require('convert-csv-to-json');
require('array.prototype.flatmap').shim()
const {Client} = require('@elastic/elasticsearch')


let fd = fs.openSync('./app/data/Data.csv', 'a+');
let buffer = new Buffer('title;seo_title;url;author;date;category;locales;content\n');
fs.writeSync(fd, buffer, 0, buffer.length, 0);

let fileInputName = './app/data/Data.csv';
let fileOutputName = './app/data/DataJSON.json';
csvToJson.generateJsonFileFromCsv(fileInputName, fileOutputName);


const client = new Client({
    node: 'http://localhost:9200'
})

async function run() {
    await client.indices.create({
        index: 'groupe_3',
        body: {
            mappings: {
                properties: {
                    title: {type: "text"},
                    seo_title: {type: "text"},
                    url: {type: "text"},
                    author: {type: "text"},
                    date: {type: "text"}, // TODO set type to date with format : MMM D, YYYY
                    category: {type: "text"},
                    locales: {type: "text"},
                    content: {type: "text"},
                }
            }
        }
    }, {ignore: [400]})

    let rawdata = fs.readFileSync('./app/data/DataJSON.json');
    const dataset = JSON.parse(rawdata);

    const body = dataset.flatMap(doc => [{index: {_index: 'groupe_3'}}, doc])

    const {body: bulkResponse} = await client.bulk({refresh: true, body})

    if (bulkResponse.errors) {
        const erroredDocuments = []
        bulkResponse.items.forEach((action, i) => {
            const operation = Object.keys(action)[0]
            if (action[operation].error) {
                erroredDocuments.push({
                    status: action[operation].status,
                    error: action[operation].error,
                    operation: body[i * 2],
                    document: body[i * 2 + 1]
                })
            }

        })
        console.log(erroredDocuments)
    }

    const {body: count} = await client.count({index: 'groupe_3'})
    console.log(count)
}

run().catch(console.log)
