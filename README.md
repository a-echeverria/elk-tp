# Node 
Install node locally
Run ```npm install``` after cloning the project

Transform data from a csv (./app/data/Data.csv) to Json and insert it in elasticsearch

Run ```"node ./app/CsvToJson"``` to execute the script

# Docker compose

First, create a .env file from .env.example file. ELK version is set to 7.4 by default.

Then you can run 

```docker-compose build```

```docker-compose up -d```
